#ifndef GDAXNETWORKREQUEST_H
#define GDAXNETWORKREQUEST_H

#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QObject>
#include <QNetworkRequest>

class GDAXNetworkRequest : public QNetworkRequest
{
public:
    GDAXNetworkRequest(QUrl url, QString apiKey, QString apiSecret, QString apiPassPhrase);
    QByteArray body() const;
    void setBody(const QByteArray &body);

    QString method() const;
    void setMethod(const QString &method);
    QNetworkReply* request(QNetworkAccessManager* nam);

private:
    void sign();
    QString m_apiKey;
    QString m_apiSecret;
    QString m_apiPassphrase;
    QString m_method = QStringLiteral("GET");
    QByteArray m_body;
};

#endif // QNETWORKREQUEST_H
