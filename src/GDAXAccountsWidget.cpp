#include "GDAXAccountsWidget.h"
#include "ui_GDAXAccountsWidget.h"
#include "GDAXAccountSettings.h"
#include <QMessageBox>

GDAXAccountsWidget::GDAXAccountsWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GDAXAccountsWidget),
    m_model(new GDAXAccountModel(this))
{
    ui->setupUi(this);
    ui->listView->setModel(m_model);
    ui->apiKeyEdit->setText(GDAXAccountSettings::instance()->apiKey());
    ui->apiSecretEdit->setText(GDAXAccountSettings::instance()->apiSecret());
    ui->apiPassphraseEdit->setText(GDAXAccountSettings::instance()->apiPassphrase());
    connect(ui->saveButton, &QPushButton::clicked, this, &GDAXAccountsWidget::saveSettings);
    connect(m_model, &GDAXAccountModel::networkError, this, &GDAXAccountsWidget::onNetworkError);
}

GDAXAccountsWidget::~GDAXAccountsWidget()
{
    delete ui;
}

void GDAXAccountsWidget::saveSettings()
{
    GDAXAccountSettings::instance()->setApiKey(ui->apiKeyEdit->text());
    GDAXAccountSettings::instance()->setApiSecret(ui->apiSecretEdit->text());
    GDAXAccountSettings::instance()->setApiPassphrase(ui->apiPassphraseEdit->text());
    GDAXAccountSettings::instance()->saveSettings();
}

void GDAXAccountsWidget::onNetworkError(QNetworkReply::NetworkError errr, QString errorString)
{
    QMessageBox::critical(this, tr("Network Error"), errorString);
}
