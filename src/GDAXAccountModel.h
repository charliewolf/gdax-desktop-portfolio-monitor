#ifndef GDAXACCOUNTMODEL_H
#define GDAXACCOUNTMODEL_H

#include <QTimer>
#include <QDateTime>
#include <QPair>
#include <QAbstractListModel>
#include <QMap>
#include <QNetworkAccessManager>
#include <QList>
#include <QNetworkReply>
#include "GDAXCurrencyAccount.h"

class GDAXAccountModel : public QAbstractListModel {
Q_OBJECT
public:
    GDAXAccountModel(QObject* parent = nullptr);
    int rowCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    void reload();
private slots:
    void onRequestFinished(QNetworkReply* reply);
    void onTickerRequestFinished(QNetworkReply* reply);
    void getInitialTickerData();
    void updateExchangeRate(GDAXCurrencyAccount::Currency currency, double price, QDateTime time);
private:
    QList<QPair<GDAXCurrencyAccount, double>> m_accounts;
    QMap<GDAXCurrencyAccount::Currency, double> m_exchangeRates;
    QNetworkAccessManager* m_qnam;
    QNetworkAccessManager* m_tickerQnam;
    QTimer m_timer;
signals:
    void networkError(QNetworkReply::NetworkError, QString);
};

#endif
