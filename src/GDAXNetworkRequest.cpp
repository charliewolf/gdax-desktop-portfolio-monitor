#include "GDAXNetworkRequest.h"
#include <QDateTime>
#include <QStringBuilder>
#include <QMessageAuthenticationCode>
#include <QCryptographicHash>

GDAXNetworkRequest::GDAXNetworkRequest(QUrl url, QString apiKey, QString apiSecret, QString apiPassPhrase) : QNetworkRequest(url)
{
    m_apiKey = apiKey;
    m_apiSecret = apiSecret;
    m_apiPassphrase = apiPassPhrase;
    setRawHeader("CB-ACCESS-KEY", m_apiKey.toUtf8());
    setRawHeader("CB-ACCESS-PASSPHRASE", m_apiPassphrase.toUtf8());
}

void GDAXNetworkRequest::sign(){
    QByteArray stringToSign = rawHeader("CB-ACCESS-TIMESTAMP") + m_method.toUpper().toUtf8() + url().path().toUtf8() + m_body;
    auto secretData = QByteArray::fromBase64(m_apiSecret.toUtf8());
    QMessageAuthenticationCode mac(QCryptographicHash::Sha256);
    mac.setKey(secretData);
    mac.addData(stringToSign);
    QByteArray signature = mac.result().toBase64();
    setRawHeader("CB-ACCESS-SIGN", signature);
}

QByteArray GDAXNetworkRequest::body() const
{
    return m_body;
}

void GDAXNetworkRequest::setBody(const QByteArray &body)
{
    m_body = body;
}

QString GDAXNetworkRequest::method() const
{
    return m_method;
}

void GDAXNetworkRequest::setMethod(const QString &method)
{
    m_method = method;
}

QNetworkReply* GDAXNetworkRequest::request(QNetworkAccessManager* nam){
    // Helper function that makes sure we handle signing/timestamp stuff correctly
    auto timestamp = QByteArray::number(QDateTime::currentDateTime().toSecsSinceEpoch());
    setRawHeader("CB-ACCESS-TIMESTAMP", timestamp);
    sign();
    return nam->sendCustomRequest(*this, m_method.toUtf8(), m_body);
}

