#ifndef GDAXACCOUNTSWIDGET_H
#define GDAXACCOUNTSWIDGET_H

#include "GDAXAccountModel.h"
#include <QWidget>

namespace Ui {
class GDAXAccountsWidget;
}

class GDAXAccountsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit GDAXAccountsWidget(QWidget *parent = 0);
    ~GDAXAccountsWidget();

private slots:
    void saveSettings();
    void onNetworkError(QNetworkReply::NetworkError, QString);

private:
    Ui::GDAXAccountsWidget *ui;
    GDAXAccountModel* m_model;
};

#endif // GDAXACCOUNTSWIDGET_H
