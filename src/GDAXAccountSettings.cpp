#include "GDAXAccountSettings.h"

GDAXAccountSettings *GDAXAccountSettings::m_instance = nullptr;

GDAXAccountSettings::GDAXAccountSettings(QObject* parent) : QObject(parent) {

}

QString GDAXAccountSettings::apiPassphrase() const
{
    return m_apiPassphrase;
}

void GDAXAccountSettings::setApiPassphrase(const QString &apiPassphrase)
{
    m_apiPassphrase = apiPassphrase;
}

void GDAXAccountSettings::readSettings()
{
    m_settings.sync();
    setApiKey(m_settings.value("apiKey").toString());
    setApiPassphrase(m_settings.value("apiPassphrase").toString());
    setApiSecret(m_settings.value("apiSecret").toString());
}

void GDAXAccountSettings::saveSettings()
{
    m_settings.setValue("apiKey", m_apiKey);
    m_settings.setValue("apiPassphrase", m_apiPassphrase);
    m_settings.setValue("apiSecret", m_apiSecret);
    m_settings.sync();
    emit settingsSaved();
}

QString GDAXAccountSettings::apiSecret() const
{
    return m_apiSecret;
}

void GDAXAccountSettings::setApiSecret(const QString &apiSecret)
{
    m_apiSecret = apiSecret;
}

QString GDAXAccountSettings::apiKey() const
{
    return m_apiKey;
}

void GDAXAccountSettings::setApiKey(const QString &apiKey)
{
    m_apiKey = apiKey;
}


GDAXAccountSettings* GDAXAccountSettings::instance(){
    if(m_instance == nullptr){
        m_instance = new GDAXAccountSettings();
    }
    return m_instance;
}


