#include "GDAXAccountModel.h"
#include "GDAXAccountSettings.h"
#include "GDAXNetworkRequest.h"
#include <QJsonDocument>
#include "GDAXCurrencyAccount.h"
#include <QJsonArray>
#include <QJsonObject>
#include <QMetaEnum>
#include "GDAXRealtimeFeed.h"

GDAXAccountModel::GDAXAccountModel(QObject* parent) : QAbstractListModel(parent), m_qnam(new QNetworkAccessManager(this)), m_tickerQnam(new QNetworkAccessManager(this))
{
    connect(m_qnam, &QNetworkAccessManager::finished, this, &GDAXAccountModel::onRequestFinished);
    connect(m_tickerQnam, &QNetworkAccessManager::finished, this, &GDAXAccountModel::onTickerRequestFinished);
    connect(GDAXRealtimeFeed::instance(), &GDAXRealtimeFeed::matchEvent, this, &GDAXAccountModel::updateExchangeRate);
    connect(GDAXAccountSettings::instance(), &GDAXAccountSettings::settingsSaved, this, &GDAXAccountModel::reload);
    reload();
    m_timer.setSingleShot(false);
    m_timer.setInterval(1000 * 60 * 2);  // Every two minutes
    connect(&m_timer, &QTimer::timeout, this, &GDAXAccountModel::reload);
}

int GDAXAccountModel::rowCount(const QModelIndex &parent) const
{

    int length = m_accounts.length();
    // We use a virtual "total" account to show the total so length will always be 1 more than the actual number of accounts
    return length == 0 ? length : length + 1;
}

QVariant GDAXAccountModel::data(const QModelIndex &index, int role) const
{

    if(role == Qt::DisplayRole){
        if(index.row() != m_accounts.length()){
            auto data = m_accounts.at(index.row());
            QString currencyKey = QMetaEnum::fromType<GDAXCurrencyAccount::Currency>().valueToKey(data.first.currency());
            double currentExchangeRate = data.second;
            double balance = data.first.value();
            if(data.first.currency() == GDAXCurrencyAccount::Currency::USD){
                return QVariant::fromValue(QString("%2: $%1").arg(balance).arg(currencyKey));
            } else if(currentExchangeRate == 0){
                return QVariant::fromValue(tr("%2: %1 - Loading Exchange Rate").arg(balance).arg(currencyKey));
            } else {
                return QVariant::fromValue(QString("%2: %1 - $%3 @ $%4").arg(balance).arg(currencyKey).arg(currentExchangeRate * balance).arg(currentExchangeRate));
            }
        } else {
                double total = 0;
                for(auto entry : m_accounts){
                    total += entry.first.value() * entry.second;
                }
                return QVariant(QString("Total: $%1").arg(total));
        }
    }
    return QVariant();
}


void GDAXAccountModel::reload()
{
    if(GDAXAccountSettings::instance()->apiKey().isEmpty()){
        return; // No point in trying to load if we don't have credentials
    }
    GDAXNetworkRequest req(QUrl("https://api.gdax.com/accounts"), GDAXAccountSettings::instance()->apiKey(), GDAXAccountSettings::instance()->apiSecret(), GDAXAccountSettings::instance()->apiPassphrase());
    auto reply = req.request(m_qnam);
}

void GDAXAccountModel::onRequestFinished(QNetworkReply* reply)
{
    if(reply->error() == QNetworkReply::NoError){
        emit layoutAboutToBeChanged();
        m_accounts.clear();
        for(auto account : QJsonDocument::fromJson(reply->readAll()).array()){
            auto accountObject = account.toObject();
            GDAXCurrencyAccount gdaxAccount(accountObject);
            m_accounts.append(QPair<GDAXCurrencyAccount, double>(gdaxAccount, gdaxAccount.currency() == GDAXCurrencyAccount::USD ? 1 : 0));
        }
        emit layoutChanged();
        getInitialTickerData();
        m_timer.start();
    } else {
        emit layoutAboutToBeChanged();
        m_accounts.clear();
        m_timer.stop();
        emit layoutChanged();
        emit networkError(reply->error(), reply->errorString());
    }
}

void GDAXAccountModel::onTickerRequestFinished(QNetworkReply *reply)
{
    if(reply->error() == QNetworkReply::NetworkError::NoError){
        auto currency = reply->property("currency").value<GDAXCurrencyAccount::Currency>();
        double price = QJsonDocument::fromJson(reply->readAll()).object().value("price").toString().toDouble();
        for(int it = 0; it < m_accounts.length(); it++){
            auto entry = m_accounts.at(it);
            if(entry.first.currency() == currency){
                m_accounts.replace(it, QPair<GDAXCurrencyAccount, double>(entry.first, price));
                emit dataChanged(index(it, 0), index(it, 1));
            }
        }
    }
}

void GDAXAccountModel::getInitialTickerData()
{
    // Get initial ticker data
    auto metaEnum = QMetaEnum::fromType<GDAXCurrencyAccount::Currency>();
    for(int i = 0; i < metaEnum.keyCount(); i++){
        QString currencyKey = QString(metaEnum.key(i)).toUpper();
        GDAXCurrencyAccount::Currency currency = (GDAXCurrencyAccount::Currency) metaEnum.value(i);
        if(currencyKey != "USD"){
            GDAXNetworkRequest req(QUrl(QString("https://api.gdax.com/products/%1-USD/ticker").arg(currencyKey)), GDAXAccountSettings::instance()->apiKey(), GDAXAccountSettings::instance()->apiSecret(), GDAXAccountSettings::instance()->apiPassphrase());
            auto reply = req.request(m_tickerQnam);
            // The response doesn't contain info on the actual product queried so we'll store this in a metaproperty so we can identify which request this came from later
            reply->setProperty("currency", currency);
        }
    }

}

void GDAXAccountModel::updateExchangeRate(GDAXCurrencyAccount::Currency currency, double price, QDateTime time){

    for(int it = 0; it < m_accounts.length(); it++){
        auto entry = m_accounts.at(it);
        if(entry.first.currency() == currency){
            m_accounts.replace(it, QPair<GDAXCurrencyAccount, double>(entry.first, price));
            emit dataChanged(index(it, 0), index(it, 1));
        }
    }
}
