#include "GDAXRealtimeFeed.h"
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QMetaEnum>
#include "GDAXCurrencyAccount.h"


GDAXRealtimeFeed *GDAXRealtimeFeed::m_instance = nullptr;

GDAXRealtimeFeed::GDAXRealtimeFeed(QObject* parent) : QWebSocket()
{
    setParent(parent);
    connect(this, &GDAXRealtimeFeed::textMessageReceived, this, &GDAXRealtimeFeed::onTextMessage);
    connect(this, &GDAXRealtimeFeed::connected, this, &GDAXRealtimeFeed::subscribe);
    connect(this, &GDAXRealtimeFeed::disconnected, this, &GDAXRealtimeFeed::start);
}


QString GDAXRealtimeFeed::createSubscribeMessage()
{
    QJsonObject message;
    message.insert("type", "subscribe");
    QJsonArray products;
    auto metaEnum = QMetaEnum::fromType<GDAXCurrencyAccount::Currency>();
    for(int i = 0; i < metaEnum.keyCount(); i++){
        QString currencyName = QString(metaEnum.key(i)).toUpper();
        if(currencyName != "USD"){
            QString productName = QString("%1-USD").arg(currencyName);
            products.append(productName);
        }
    }
    message.insert("product_ids", products);
    QJsonDocument result;
    result.setObject(message);
    return QString::fromUtf8(result.toJson());
}

void GDAXRealtimeFeed::start()
{
    open(QUrl("wss://ws-feed.gdax.com"));

}

GDAXRealtimeFeed* GDAXRealtimeFeed::instance()
{
    if(m_instance == nullptr){
        m_instance = new GDAXRealtimeFeed();
    }
    return m_instance;
}

void GDAXRealtimeFeed::subscribe()
{
    sendTextMessage(createSubscribeMessage());
}

void GDAXRealtimeFeed::onTextMessage(QString msg)
{
    QJsonObject event = QJsonDocument::fromJson(msg.toUtf8()).object();
    if(event.value("type").toString() == "match"){
        QStringList currencyPair = event.value("product_id").toString().split("-");
        QString crypto = currencyPair.at(0).toUpper();
        QDateTime time = QDateTime::fromString(event.value("time").toString(), Qt::ISODateWithMs);
        GDAXCurrencyAccount::Currency currency =  (GDAXCurrencyAccount::Currency) QMetaEnum::fromType<GDAXCurrencyAccount::Currency>().keyToValue(crypto.toLocal8Bit());
        double rate = event.value("price").toString().toDouble(); //GDAX returns the rates as a string
        emit matchEvent(currency, rate, time);
    }
}
