#ifndef GDAXCURRENCYACCOUNT_H
#define GDAXCURRENCYACCOUNT_H

#include <QObject>
#include <QJsonObject>

class GDAXCurrencyAccount
{
    Q_GADGET
    Q_PROPERTY(Currency currency READ currency)
    Q_PROPERTY(double value READ value)
public:
    enum Currency { BTC, LTC, ETH, USD };
    Q_ENUM(Currency)

    GDAXCurrencyAccount();
    GDAXCurrencyAccount(Currency currency);
    GDAXCurrencyAccount(QJsonObject accountObject);

    double value() const;
    void setValue(double value);

    Currency currency() const;
    void setCurrency(const Currency &currency);

private:
    Currency m_currency;
    double m_value;
};

Q_DECLARE_METATYPE(GDAXCurrencyAccount)

#endif // GDAXCURRENCYACCOUNT_H
