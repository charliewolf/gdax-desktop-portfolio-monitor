#include "GDAXNetworkRequest.h"
#include  <QNetworkAccessManager>
#include <QEventLoop>
#include <QObject>
#include <QDebug>
#include <QApplication>
#include "GDAXRealtimeFeed.h"
#include "GDAXAccountSettings.h"
#include "GDAXAccountsWidget.h"

int main(int argc, char** argv){

    QApplication app(argc, argv);
    QCoreApplication::setOrganizationDomain("is.wolf.toys.crypto");
    QCoreApplication::setApplicationName("GDAXMonitor");
    auto settings = GDAXAccountSettings::instance();
    settings->readSettings();
    GDAXRealtimeFeed::instance()->start();
    GDAXAccountsWidget mainWin;
    mainWin.show();
    app.exec();
}
