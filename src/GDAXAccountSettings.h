#pragma once
#include <QObject>
#include <QSettings>

class GDAXAccountSettings : public QObject {
  Q_OBJECT
  public:
     static GDAXAccountSettings *instance();
     QString apiKey() const;
     void setApiKey(const QString &apiKey);

     QString apiSecret() const;
     void setApiSecret(const QString &apiSecret);

     QString apiPassphrase() const;
     void setApiPassphrase(const QString &apiPassphrase);

     void readSettings();

     void saveSettings();

signals:
     void settingsSaved();

private:
     explicit GDAXAccountSettings(QObject* parent = 0);
     static GDAXAccountSettings* m_instance;
     QString m_apiKey;
     QString m_apiSecret;
     QString m_apiPassphrase;
     QSettings m_settings;
};
