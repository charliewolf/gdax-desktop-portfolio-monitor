#ifndef GDAXREALTIMEFEED_H
#define GDAXREALTIMEFEED_H

#include "GDAXCurrencyAccount.h"
#include <QWebSocket>
#include <QObject>
#include <QDateTime>

class GDAXRealtimeFeed : public QWebSocket
{
Q_OBJECT
public:
    void start();
    static GDAXRealtimeFeed* instance();
private slots:
    void subscribe();
    void onTextMessage(QString);
private:
    static GDAXRealtimeFeed* m_instance;
    GDAXRealtimeFeed(QObject* parent = nullptr);
    QString createSubscribeMessage();
signals:
    void matchEvent(GDAXCurrencyAccount::Currency currency, double price, QDateTime time);

};

#endif // GDAXREALTIMEFEED_H
