#include "GDAXCurrencyAccount.h"
#include <QMetaEnum>
#include <QDebug>

GDAXCurrencyAccount::GDAXCurrencyAccount()
{

}

GDAXCurrencyAccount::GDAXCurrencyAccount(GDAXCurrencyAccount::Currency currency)
{
    setCurrency(currency);
}

GDAXCurrencyAccount::GDAXCurrencyAccount(QJsonObject accountObject)
{
    int value = QMetaEnum::fromType<GDAXCurrencyAccount::Currency>().keyToValue(accountObject.value("currency").toString().toLocal8Bit());
    if(value == -1){
       qDebug() << "Bad currency"; // TODO throw execption here
    } else {
       GDAXCurrencyAccount((GDAXCurrencyAccount::Currency) value);
       setCurrency((GDAXCurrencyAccount::Currency) value);
    }
    setValue(accountObject.value("balance").toString().toDouble());
}

double GDAXCurrencyAccount::value() const
{
    return m_value;
}

void GDAXCurrencyAccount::setValue(double value)
{
    m_value = value;
}

GDAXCurrencyAccount::Currency GDAXCurrencyAccount::currency() const
{
    return m_currency;
}

void GDAXCurrencyAccount::setCurrency(const Currency &currency)
{
    m_currency = currency;
}
